import paho.mqtt.client as mqtt
import json
#pass
# import paho.mqtt.subscribe as msg
broker="13.67.108.80"
port = 1883

def on_connect(client, userdata,flags ,rc):
    print("MQTT Connected.")
    
def on_message(client, userdata,msg):
    print("MQTT Got Message"+msg.payload.decode())
    x = msg.payload.decode()
    y =json.loads(x)
    print(y["SW"])
    
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set("admin","admin")
client.connect(broker,port)
client.subscribe("dashboards/data/11668450-ca26-11e9-a06d-290cc5e841d6",qos=1)
client.message_callback_add("dashboards/data/11668450-ca26-11e9-a06d-290cc5e841d6", on_message)
client.loop_forever()    