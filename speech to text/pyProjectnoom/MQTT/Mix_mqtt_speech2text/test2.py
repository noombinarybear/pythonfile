import paho.mqtt.client as mqtt  #import the client1
import time
import json
payload =json.dumps({"DeviceId":"1222e830-39a7-11e9-a626-57ec0ce09abe", "Sensors":[ { "Name":"Temp", "Data":"30" }, { "Name":"Hum", "Data":"60" } ] })
# เทสคอนเน็ค ifarm mqtt
# ip: 13.67.108.80  port:1883
def on_connect(client, userdata, flags, rc):
    if rc==0:
        # client.publish("monitor/11668450-ca26-11e9-a06d-290cc5e841d6",payload)  ####เพิ่ม 1
        client.connected_flag=True #set flag
        print("connected OK")
        print(payload)
    else:
        print("Bad connection Returned code=",rc)

mqtt.Client.connected_flag=False#create flag in class
broker="13.67.108.80"
client = mqtt.Client("python1")             #create new instance 
client.username_pw_set("admin","admin")
client.on_connect=on_connect  #bind call back function
client.loop_start()
print("Connecting to broker ",broker)
client.connect(broker,1883)      #connect to broker
while not client.connected_flag: #wait in loop
    print("In wait loop")
    time.sleep(1)
print("in Main Loop")
client.publish('monitor/11668450-ca26-11e9-a06d-290cc5e841d6',payload)  ####เพิ่ม 1
print("PUB!!")
client.loop_stop()    #Stop loop 
# client.disconnect() # disconnect