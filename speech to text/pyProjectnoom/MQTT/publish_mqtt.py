import paho.mqtt.client as mqtt  #import the client1 #passs
# import paho.mqtt.subscribe as subscribe
import random
import time
import json
broker="13.67.108.80"
port=1883
randomnum=random.randint(0,6) #สุ่มเลข
payload =json.dumps({"DeviceId":"11668450-ca26-11e9-a06d-290cc5e841d6", "Sensors":[ { "Name":"Temp", "Data":randomnum }, { "Name":"Hum", "Data":"60" } ] })

def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass

# def on_message_print(client, userdata, message):
#     print("%s %s" % (message))
# subscribe.callback(on_message_print, "dashboards/data/11668450-ca26-11e9-a06d-290cc5e841d6",hostname=broker,qos=1)    

client= mqtt.Client("control1")  
client.username_pw_set("admin","admin")                         #create client object
client.on_publish = on_publish                          #assign function to callback
client.connect(broker,port)                                 #establish connection
ret= client.publish('monitor/11668450-ca26-11e9-a06d-290cc5e841d6',payload) 