﻿# -*- coding: utf-8 -*-
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

chatbot = ChatBot(
    'Fah', # ชื่อแชตบ็อต
	read_only = True,
	logic_adapters=[
        {
			"import_path": "chatterbot.logic.BestMatch",
            "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
            "response_selection_method": "chatterbot.response_selection.get_random_response"
        }
    ],
    storage_adapter='chatterbot.storage.SQLStorageAdapter', # กำหนดการจัดเก็บ ในที่นี้เลือก chatterbot.storage.SQLStorageAdapter เก็บเป็น Sqllite
    database='fah.sqlite3' # ที่ตั้งฐานข้อมูล
)

chatbot.set_trainer(ChatterBotCorpusTrainer) # กำหนดให้ Train จากชุดข้อมูลของ Chatterbot
chatbot.train(
    "chatterbot.corpus.thai"
)
text=""
while True:
    text=input("นายท่าน : ")
    if text=="exit":
        break
    response = chatbot.get_response(text)
    print(response)