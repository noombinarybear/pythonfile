﻿import speech_recognition as sr 
import speak
from chatterbot import ChatBot
r = sr.Recognizer() 
r.energy_threshold = 500
chatbot = ChatBot(
    'Fah', # ชื่อแชตบ็อต
	read_only = True,
	logic_adapters=[
        {
			"import_path": "chatterbot.logic.BestMatch",
            "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
            "response_selection_method": "chatterbot.response_selection.get_random_response"
        },
        {
           'import_path': 'chatterbot.logic.LowConfidenceAdapter',
           'threshold': 0.6,
           'default_response': 'แย้มงงคะนายท่าน..'
        }
    ],
    storage_adapter='chatterbot.storage.SQLStorageAdapter', # กำหนดการจัดเก็บ ในที่นี้เลือก chatterbot.storage.SQLStorageAdapter เก็บเป็น Sqllite
    database='fah.sqlite3' # ที่ตั้งฐานข้อมูล
)
while True:
	with sr.Microphone() as source:
		print ('Say Something!')
		audio = r.listen(source)
		print ('Done!')
		try:
			text = r.recognize_google(audio, language = "th-TH")
			response = chatbot.get_response(text)
			print('You said:\n' + text)
			lang = 'th'
			print(response)
			speak.tts(text , lang)
		except Exception as e:
			print (e)
